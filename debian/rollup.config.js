const node = require("@rollup/plugin-node-resolve").nodeResolve;
const { babel } = require('@rollup/plugin-babel');
const cjs = require('@rollup/plugin-commonjs');

const config = {
  output: {
    indent: false,
    extend: true,
  },
  plugins: [
    babel({babelHelpers:'bundled'}),
    node(
     {
        modulePaths: ['/usr/share/nodejs', '/usr/lib/nodejs', 'node_modules'],
        preferBuiltins: false
     }
    ),
    cjs(),
  ]
};

module.exports = [
  config
];
